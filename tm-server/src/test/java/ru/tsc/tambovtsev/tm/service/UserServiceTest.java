package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.repository.dto.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.dto.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.service.dto.UserService;
import ru.tsc.tambovtsev.tm.util.HashUtil;

public final class UserServiceTest {

    private static IPropertyService propertyService;

    private static IConnectionService connectionService;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private IUserService userService;

    @Nullable
    private UserDTO user;

    @BeforeClass
    public static void initConnection() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @AfterClass
    public static void disconnectConnection() {
        connectionService.close();
    }

    @Before
    public void setUserService() {
        projectRepository = new ProjectRepository(connectionService.getEntityManager());
        taskRepository = new TaskRepository(connectionService.getEntityManager());
        userRepository = new UserRepository(connectionService.getEntityManager());
        userService = new UserService(
                connectionService, userRepository,  propertyService, projectRepository, taskRepository
        );
        user = new UserDTO();
        user.setEmail("testUser1@test.ru");
        user.setLogin("testUser1");
        user.setPasswordHash(HashUtil.salt(propertyService, "1212325346"));
        userService.create(user);
        user = new UserDTO();
        user.setEmail("testUser2@test.ru");
        user.setLogin("testUser2");
        user.setPasswordHash(HashUtil.salt(propertyService, "0394560983405968349568"));
        userService.create(user);
    }

    @After
    public void clearUserService() {
        userService.removeByLogin("testUser1");
        userService.removeByLogin("testUser2");
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(userService.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertFalse(userService.findAll().get(0).getId().isEmpty());
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO findUser =  userService.findByLogin("testUser1");
        Assert.assertFalse(findUser.getLogin().isEmpty());
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO findUser =  userService.findByEmail("testUser1@test.ru");
        Assert.assertFalse(findUser.getEmail().isEmpty());
    }

    @Test
    public void testRemoveById() {
        @Nullable final UserDTO findUser =  userService.findAll().get(0);
        userService.removeById(findUser.getId());
        Assert.assertNotEquals(userService.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final UserDTO findUser =  userService.findAll().get(0);
        userService.removeByLogin(findUser.getLogin());
        Assert.assertNotEquals(userService.findAll().get(0).getId(), findUser.getId());
    }

    @Test
    public void testExistByLogin() {
        @Nullable final UserDTO findUser =  userService.findAll().get(0);
        Assert.assertTrue(userService.isLoginExists(findUser.getLogin()));
    }

    @Test
    public void testUpdateByUser() {
        @Nullable final UserDTO findUser =  userService.findAll().get(0);
        findUser.setLogin("testUser3");
        userService.update(findUser);
        Assert.assertNotNull(userService.findByLogin("testUser3"));
    }

    @Test
    public void testSetPassword() {
        @Nullable final UserDTO user = new UserDTO();
        user.setPasswordHash(userService.findAll().get(0).getPasswordHash());
        user.setEmail(userService.findAll().get(0).getEmail());
        user.setId(userService.findAll().get(0).getId());
        user.setLogin(userService.findAll().get(0).getLogin());
        userService.setPassword(user.getId(), userService.findAll().get(1).getPasswordHash());
        Assert.assertNotEquals(user.getPasswordHash(), userService.findAll().get(0).getPasswordHash());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(NullPointerException.class, () -> userService.create(null));
    }

}
